﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form, bileşen listesini temizlemeyi bırakmayı geçersiz kılar.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows Form Tasarımcısı tarafından gerektirilir
    Private components As System.ComponentModel.IContainer

    'NOT: Aşağıdaki yordam Windows Form Tasarımcısı için gereklidir
    'Windows Form Tasarımcısı kullanılarak değiştirilebilir.  
    'Kod düzenleyicisini kullanarak değiştirmeyin.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_insertProductName = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_insertProductDesc = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_insertProductStockPrice = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_insertProductStockCount = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_insertProductSellPrice = New System.Windows.Forms.TextBox()
        Me.btnAddProduct = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnAddProduct)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txt_insertProductSellPrice)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txt_insertProductStockCount)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txt_insertProductStockPrice)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txt_insertProductDesc)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txt_insertProductName)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(254, 283)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Ürün Ekle"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Ürün Adı"
        '
        'txt_insertProductName
        '
        Me.txt_insertProductName.Location = New System.Drawing.Point(6, 36)
        Me.txt_insertProductName.Name = "txt_insertProductName"
        Me.txt_insertProductName.Size = New System.Drawing.Size(242, 20)
        Me.txt_insertProductName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Ürün Açıklaması"
        '
        'txt_insertProductDesc
        '
        Me.txt_insertProductDesc.Location = New System.Drawing.Point(6, 80)
        Me.txt_insertProductDesc.Name = "txt_insertProductDesc"
        Me.txt_insertProductDesc.Size = New System.Drawing.Size(242, 20)
        Me.txt_insertProductDesc.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 111)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Ürün Stok Fiyatı"
        '
        'txt_insertProductStockPrice
        '
        Me.txt_insertProductStockPrice.Location = New System.Drawing.Point(6, 127)
        Me.txt_insertProductStockPrice.Name = "txt_insertProductStockPrice"
        Me.txt_insertProductStockPrice.Size = New System.Drawing.Size(242, 20)
        Me.txt_insertProductStockPrice.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 155)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(82, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Ürün Stok Adeti"
        '
        'txt_insertProductStockCount
        '
        Me.txt_insertProductStockCount.Location = New System.Drawing.Point(6, 171)
        Me.txt_insertProductStockCount.Name = "txt_insertProductStockCount"
        Me.txt_insertProductStockCount.Size = New System.Drawing.Size(242, 20)
        Me.txt_insertProductStockCount.TabIndex = 6
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 196)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(83, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Ürün Satış Fiyatı"
        '
        'txt_insertProductSellPrice
        '
        Me.txt_insertProductSellPrice.Location = New System.Drawing.Point(6, 212)
        Me.txt_insertProductSellPrice.Name = "txt_insertProductSellPrice"
        Me.txt_insertProductSellPrice.Size = New System.Drawing.Size(242, 20)
        Me.txt_insertProductSellPrice.TabIndex = 8
        '
        'btnAddProduct
        '
        Me.btnAddProduct.Location = New System.Drawing.Point(6, 238)
        Me.btnAddProduct.Name = "btnAddProduct"
        Me.btnAddProduct.Size = New System.Drawing.Size(110, 23)
        Me.btnAddProduct.TabIndex = 10
        Me.btnAddProduct.Text = "Ürünü Oluştur"
        Me.btnAddProduct.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(780, 465)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_insertProductName As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txt_insertProductStockCount As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_insertProductStockPrice As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_insertProductDesc As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txt_insertProductSellPrice As TextBox
    Friend WithEvents btnAddProduct As Button
End Class
