﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Bir bütünleştirilmiş koda ilişkin Genel Bilgiler aşağıdaki öznitelikler kümesiyle
' denetlenir. Bütünleştirilmiş kod ile ilişkili bilgileri değiştirmek için
' bu öznitelik değerlerini değiştirin.

' Bütünleştirilmiş kod özniteliklerinin değerlerini gözden geçirin

<Assembly: AssemblyTitle("AccessDatabase")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("")>
<Assembly: AssemblyProduct("AccessDatabase")>
<Assembly: AssemblyCopyright("Copyright ©  2019")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'Bu proje COM'un kullanımına sunulursa aşağıdaki GUID, typelib kimliği içindir
<Assembly: Guid("951df1a2-cccf-462e-ae3b-60079712e274")>

' Bir derlemenin sürüm bilgileri aşağıdaki dört değerden oluşur:
'
'      Ana Sürüm
'      İkincil Sürüm 
'      Yapı Numarası
'      Düzeltme
'
' Tüm değerleri belirtebilir veya varsayılan Derleme ve Düzeltme Numaralarını kullanmak için
' aşağıda gösterildiği gibi '*' kullanabilirsiniz:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.0.0.0")>
<Assembly: AssemblyFileVersion("1.0.0.0")>
